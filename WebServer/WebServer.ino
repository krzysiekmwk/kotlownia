/*
  Web Server

 A simple web server that shows the value of the analog input pins.
 using an Arduino Wiznet Ethernet shield.

 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 * Analog inputs attached to pins A0 through A5 (optional)

 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe
 modified 02 Sept 2015
 by Arturo Guadalupi

 */

#include <SPI.h>
#include <Ethernet.h>

int values[2];
float temperatures[5] = {10.4, 12.4, 34.5, 65, 23};

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // start the Ethernet connection and the server:
  Ethernet.begin(mac);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}


void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        String request = client.readStringUntil('\r');
        if(strstr(request.c_str(),"s1")){
          for(int i = 0; i < 2; i++){
            int index = request.indexOf('=') + 1;
            request = request.substring(index);
            values[i] = atoi(request.substring(0,3).c_str()) - 100; //pamietac ze wartosc jest od 100 do 200;
            Serial.println(values[i]);
          }
        }
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          //client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");

          //tabelka z temperatur
          client.println(""); 
          client.println("<table border='1' style='width:100%'>");
          client.println("<tr>"
              "<th>kociol</th>"
              "<th>piec</th>" 
              "<th>komputer</th>" 
              "<th>kot</th>" 
              "<th>teleskop</th>" 
            "</tr>");

          client.println("<tr>");
          client.println("<td>");client.println(temperatures[0]);client.println("</td>");
          client.println("<td>");client.println(temperatures[1]);client.println("</td>");
          client.println("<td>");client.println(temperatures[2]);client.println("</td>");
          client.println("<td>");client.println(temperatures[3]);client.println("</td>");
          client.println("<td>");client.println(temperatures[4]);client.println("</td>");
          client.println("</tr>");

            
          client.println("</table>");

          client.println("</br></br></br></br></br></br></br></br></br></br></br></br>");

          client.println("<form method='GET'>");

          client.println("<table border='1' style='width:100%' >");
          client.println("<tr>"
              "<th>First servo</th>"
              "<th>Second</th>" 
            "</tr>");

          //wartosci poszczegolnych serw
          client.print("<tr>");
          client.print("<td>");
          client.print(values[0]);
          client.print("</td>");
          
          client.print("<td>");
          client.print(values[1]);
          client.print("</td>");
          client.print("</tr>");
          
          client.println("<tr>");
          //slider one
          client.println("<td>");
          client.print("<input type='range' name='s1' min='100' max='200' step='1' onChange='submit();' style='width:90%' value='");
          client.print(values[0] + 100);
          client.println("'/>");
          client.println("</td>");

          //slider two
          client.println("<td>");
          client.print("<input type='range' name='s2' min='100' max='200' step='1' onChange='submit();' style='width:90%' value='");
          client.print(values[1] + 100);
          client.println("'/>");
          client.println("</td>");

          client.println("</tr>");

          client.println("</table>");

          client.println("</form>");
          client.println("</html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}

