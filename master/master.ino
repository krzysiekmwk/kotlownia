#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define PR_PIN 0

const int slaveThermometers = 0x02;
const int slaveServos = 0x03;
const int master = 0x05;
const int countOfThermometers = 30;

const int pinFirstPump = 2;
const int pinSecondPump = 3;
const int pinThirdPump = 4;

const int pinToManualControlFirstPump = 5;
const int pinToManualControlFirstPumpLed = 6;
const int pinToManualControlSecondPump = 7;
const int pinToManualControlSecondPumpLed = 8;
const int pinToManualControlThirdPump = 9;
const int pinToManualControlThirdPumpLed = 10;
const int pinToCriticalTemperatureLed = 11;  

bool manualControlFirstPump = 0;
bool manualControlSecondPump = 0;
bool manualControlThirdPump = 0;

int criticalTemperature = 0;
int temperature[countOfThermometers];
int pinToDisplayControl = 12;
int screenCounter = 0;
int buttonState = 0;


LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);


void sendDataToServo(int chosenServo, int rotationInPercentage, int criticalTemperature);
int8_t readTemperature(int thermometerNumber);
void pumpSection(void);
void servosSection(void);
void buttonSection(void);
void refreshLEDSection(void);
void checkTemperatureSection(void);
void checkCriticalValue(void);

void setup() {
	Wire.begin(master);		// join i2c bus (master)
	Serial.begin(9600);		// start serial for output

  lcd.begin(20,4);
  lcd.backlight();
   
  pinMode(pinFirstPump, OUTPUT);
  pinMode(pinSecondPump, OUTPUT);
  pinMode(pinThirdPump, OUTPUT);
  pinMode(pinToManualControlFirstPump, INPUT_PULLUP);
  pinMode(pinToManualControlFirstPumpLed, OUTPUT);
  pinMode(pinToManualControlSecondPump, INPUT_PULLUP);
  pinMode(pinToManualControlSecondPumpLed, OUTPUT);
  pinMode(pinToManualControlThirdPump, INPUT_PULLUP);
  pinMode(pinToManualControlThirdPumpLed, OUTPUT);
  pinMode(pinToCriticalTemperatureLed, OUTPUT); //20161016
  pinMode(pinToDisplayControl, INPUT_PULLUP); //20161016

  
}

void loop() {
	
	checkTemperatureSection();
	checkCriticalValue();
	buttonSection();
	refreshLEDSection();
	pumpSection();
  servosSection();     
  displayControl(); //20161016
  
	delay(100);
 }

void sendDataToServo(int chosenServo, int rotationInPercentage, int criticalTemperature) {
	Wire.beginTransmission(slaveServos);
	Wire.write(chosenServo);
	Wire.write(rotationInPercentage);
  Wire.write(criticalTemperature);
	Wire.endTransmission();
}

int8_t readTemperature(int thermometerNumber) {
	int8_t temperature;
	do {
		Serial.print(thermometerNumber);
		Serial.print(" ");
		Wire.beginTransmission(slaveThermometers);
		Wire.write(thermometerNumber);
		Wire.endTransmission();

		Wire.requestFrom(slaveThermometers, 1);
		temperature = Wire.read();
		Serial.println(temperature);

	} while (temperature == -1);

	return temperature;
}

void checkTemperatureSection(){
	for (int i=1; i<=countOfThermometers; i++) {
    temperature[i]=readTemperature(i);
  }
}

void buttonSection(){
	if (digitalRead(pinToManualControlFirstPump) == LOW )
  {
    manualControlFirstPump = !manualControlFirstPump;
    digitalWrite(pinToManualControlFirstPumpLed, manualControlFirstPump);
    delay(1500);
  }

if (digitalRead(pinToManualControlSecondPump) == LOW )
  {
    manualControlSecondPump = !manualControlSecondPump;
    digitalWrite(pinToManualControlSecondPumpLed, manualControlSecondPump);
    delay(1500);
  }

if (digitalRead(pinToManualControlThirdPump) == LOW )
  {
    manualControlThirdPump = !manualControlThirdPump;
    digitalWrite(pinToManualControlThirdPumpLed, manualControlThirdPump);
    delay(1500);
  }
}

void checkCriticalValue(){
	//critical temperature
  
 if (readTemperature(1) >= 90)
    { 
    criticalTemperature  = 1;
    }
 else
    {
    criticalTemperature  = 0;
    }
}

void refreshLEDSection(){
  digitalWrite(pinToManualControlFirstPumpLed, manualControlFirstPump); // odswiezenie diod gdy tryb manuany przejdzie w krytyczny
  digitalWrite(pinToManualControlSecondPumpLed, manualControlSecondPump);
  digitalWrite(pinToManualControlThirdPumpLed, manualControlThirdPump);
}

void pumpSection(){
	 //pump1
  if (criticalTemperature  == 1 or readTemperature(1) > 60 and manualControlFirstPump == 0 )
    {
   digitalWrite(pinFirstPump, HIGH);
    }
  else if (criticalTemperature  == 0 and readTemperature(1) < 60 and manualControlFirstPump == 1 )
    {
    digitalWrite(pinFirstPump, HIGH);
    }
  else
    {
   digitalWrite(pinFirstPump, LOW);
    }
//pump2      
  if (criticalTemperature  == 1 or readTemperature(1) > 60 and manualControlSecondPump == 0)
    {
   digitalWrite(pinSecondPump, HIGH);
    }
  else if (criticalTemperature  == 0 and readTemperature(1) < 60 and manualControlSecondPump == 1)
    {
   digitalWrite(pinSecondPump, HIGH);
    } 
  else
    {
   digitalWrite(pinSecondPump, LOW);
    }
//pump3
  if (criticalTemperature  == 1 or readTemperature(1) > 50 and manualControlThirdPump == 0)
    {
   digitalWrite(pinThirdPump, HIGH);
    }
  else if (criticalTemperature  == 0 and readTemperature(1) < 50 and manualControlThirdPump == 1) 
    {
   digitalWrite(pinThirdPump, HIGH);
    } 
  else 
    {
   digitalWrite(pinThirdPump, LOW);
    }
}

void servosSection(){
	//servos1
  if (readTemperature(1) > 40)
    sendDataToServo(1, 100,criticalTemperature);
  else
    sendDataToServo(1, 0,criticalTemperature);

//servos2
  if (readTemperature(16) > readTemperature(17) )
     sendDataToServo(2, 100,criticalTemperature);
  else
     sendDataToServo(2, 0,criticalTemperature);
     
//servos3
  if (readTemperature(7) >= readTemperature(3) or readTemperature(22) > readTemperature(3))
    sendDataToServo(3,100,criticalTemperature);
  
  else
    sendDataToServo(3,0,criticalTemperature);

//servos4

  if (readTemperature(19) > 65  and readTemperature(21) <readTemperature(19))
  {
    sendDataToServo(4,100,criticalTemperature);
  }
  else if (readTemperature(21) == readTemperature(19))
  {
    sendDataToServo(4,50,criticalTemperature);
  }
  else
  {
    sendDataToServo(4,0,criticalTemperature);
  }
}


void displayControl(){
  buttonState=digitalRead(pinToDisplayControl);
  if(buttonState == 0){
    screenCounter++;
  }
  if(screenCounter == 0){
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("TEMP. PIECA");
    lcd.setCursor(0,1);
    lcd.print("WYJSCIE");
    lcd.setCursor(0,2);
    lcd.print("WEJSCIE");
    lcd.setCursor(8,1);
    lcd.print(readTemperature(1));
    lcd.setCursor(10,1);
    lcd.print((char)223);
    lcd.setCursor(11,1);
    lcd.print("C");
    lcd.setCursor(8,2);
    lcd.print(readTemperature(2));  
    lcd.setCursor(10,2);
    lcd.print((char)223);
    lcd.setCursor(11,2);
    lcd.print("C");
    }

  if(screenCounter == 1){
    lcd.clear();
    lcd.setCursor(2,0);
    lcd.print("TEMP. ZASOBNIKOW ");
    lcd.setCursor(8,1);
    lcd.print("I");
    lcd.setCursor(15,1);
    lcd.print("II");
    lcd.setCursor(0,2);
    lcd.print("GORA");
    lcd.setCursor(0,3);
    lcd.print("DOL");
    lcd.setCursor(8,2);
    lcd.print(readTemperature(21));
    lcd.setCursor(10,2);
    lcd.print((char)223);
    lcd.setCursor(11,2);
    lcd.print("C");
    lcd.setCursor(8,3);
    lcd.print(readTemperature(22));
    lcd.setCursor(10,3);
    lcd.print((char)223);
    lcd.setCursor(11,3);
    lcd.print("C");
    lcd.setCursor(15,2);
    lcd.print(readTemperature(19));
    lcd.setCursor(17,2);
    lcd.print((char)223);
    lcd.setCursor(18,2);
    lcd.print("C");
    lcd.setCursor(15,3);
    lcd.print(readTemperature(20));
    lcd.setCursor(17,3);
    lcd.print((char)223);
    lcd.setCursor(18,3);
    lcd.print("C"); 
    }
    if(screenCounter == 2){
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("WYMIENNIKI ");
    lcd.setCursor(4,1);
    lcd.print("*");
    lcd.setCursor(5,1);
    lcd.print("*");
    lcd.setCursor(14,1);
    lcd.print("*");
    lcd.setCursor(15,1);
    lcd.print("*");
    lcd.setCursor(4,3);
    lcd.print("*");
    lcd.setCursor(5,3);
    lcd.print("*");
    lcd.setCursor(14,3);
    lcd.print("*");
    lcd.setCursor(15,3);
    lcd.print("*");
    lcd.setCursor(3,2);
    lcd.print("GORA");
    lcd.setCursor(12,2);
    lcd.print("GLIKOL");
    lcd.setCursor(2,1);
    lcd.print(readTemperature(3));
    lcd.setCursor(6,1);
    lcd.print(readTemperature(11));
    lcd.setCursor(12,1);
    lcd.print(readTemperature(4));
    lcd.setCursor(16,1);
    lcd.print(readTemperature(16));
    lcd.setCursor(2,3);
    lcd.print(readTemperature(4));
    lcd.setCursor(6,3);
    lcd.print(readTemperature(13));
    lcd.setCursor(12,3);
    lcd.print(readTemperature(5));
    lcd.setCursor(16,3);
    lcd.print(readTemperature(17)); 
    } 
    
  if(screenCounter == 4){

    screenCounter =0;
  }




  
}

