#include <Wire.h>
#include <Servo.h> 

//All servos
//*******************************************
Servo valveFirst;
int valveFirstPin = 3;
int positionFirst = 0;
int correctionFirst = 0;

Servo valveSecond;
int valveSecondPin = 5;
int positionSecond = 0;
int correctionSecond = 0;

Servo valveThird;
int valveThirdPin = 6;
int positionThird = 0;
int correctionThird = 0;

Servo valveFourth;
int valveFourthPin = 9;
int positionFourth = 0;
int correctionFourth = 0;
//*******************************************

const int slaveServos = 0x03;
const int master = 0x05;

bool manualControlFirsServo = 0;
const int pinToManualControlFirstServo = 2; //pin do którego jest wpięty switch (połączenie: masa -> switch -> pin)
const int pinToManualControlFirstServoLED = 4; // pin na którym diody będą się zapalać, sygnalizujac reczna kontrole
//pomyslec nad zrobieniem podklasy servos w ktorej to kazdy obiekt bedzie dodatkowo trzymal ustawiony pin itp, zwykle dziedziczenie

bool manualControlSecondServo = 0;
bool manualControlThirdServo = 0;
bool manualControlFourthServo = 0;

const int pinToManualControlSecondServo = 7;
const int pinToManualControlSecondServoLED = 8;
const int pinToManualControlThirdServo = 10;
const int pinToManualControlThirdServoLED = 11;
const int pinToManualControlFourthServo = 12;
const int pinToManualControlFourthServoLED = 13;



void setup() {
	Wire.begin(slaveServos);
	setAllServos();
	Serial.begin(9600);
	Wire.onReceive(setValve);
	
	pinMode(pinToManualControlFirstServo, INPUT_PULLUP);
	pinMode(pinToManualControlFirstServoLED, OUTPUT);
	digitalWrite(pinToManualControlFirstServoLED, manualControlFirsServo); //dioda się świeci tylko w trybie manualnym. Zakładam że HIGH zaświeci diodę

  pinMode(pinToManualControlSecondServo, INPUT_PULLUP);
  pinMode(pinToManualControlSecondServoLED, OUTPUT);
  digitalWrite(pinToManualControlSecondServoLED, manualControlSecondServo);

  pinMode(pinToManualControlThirdServo, INPUT_PULLUP);
  pinMode(pinToManualControlThirdServoLED, OUTPUT);
  digitalWrite(pinToManualControlThirdServoLED, manualControlThirdServo);

  pinMode(pinToManualControlFourthServo, INPUT_PULLUP);
  pinMode(pinToManualControlFourthServoLED, OUTPUT);
  digitalWrite(pinToManualControlFourthServoLED, manualControlFourthServo);

}

void loop() {
	if(digitalRead(pinToManualControlFirstServo) == LOW)
  	  {
		  manualControlFirsServo = !manualControlFirsServo;
		  digitalWrite(pinToManualControlFirstServoLED, manualControlFirsServo);
		  delay(500);
		  } 
	if(digitalRead(pinToManualControlSecondServo) == LOW)
		  {
      manualControlSecondServo = !manualControlSecondServo;
      digitalWrite(pinToManualControlSecondServoLED, manualControlSecondServo);
      delay(500);
      } 
   if (digitalRead(pinToManualControlThirdServo) == LOW)
      {
      manualControlThirdServo = !manualControlThirdServo;
      digitalWrite(pinToManualControlThirdServoLED, manualControlThirdServo);
      delay(500);
      }
  if (digitalRead(pinToManualControlFourthServo) == LOW)
      {
      manualControlFourthServo = !manualControlFourthServo;
      digitalWrite(pinToManualControlFourthServoLED, manualControlFourthServo);
      delay(500);
     }

	if (manualControlFirsServo)
		{
		  int angleFirst = map(analogRead(0), 0, 1023, 0, 90);
      setServo(1, angleFirst);
    }
  if (manualControlSecondServo)		
		{
		  int angleSecond = map(analogRead(1), 0, 1023, 0, 90); //dla kazdego osobny if
      setServo(2, angleSecond);
		}
	if (manualControlThirdServo)	
    {
		  int angleThird = map(analogRead(2), 0, 1023, 0, 90);
	    setServo(3, angleThird);
    }
	if (manualControlFourthServo)	
  {
		int angleFourth = map(analogRead(3), 0, 1023, 0, 180);
		setServo(4, angleFourth);
	}
	
	
	
}

void setServo(int pin, int value) {
	switch (pin){
        case 1:
			valveFirst.write(value);
			break;
        
        case 2:
			valveSecond.write(value);
			break;
        
        case 3:
			valveThird.write(value);
			break;
        
        case 4:
			valveFourth.write(value);
			break;
        
        default:
			break;
      
   }
digitalWrite(pinToManualControlFirstServoLED, manualControlFirsServo); 
}

void setValve(int numBytes) {
	int pinNumber = Wire.read();
	int value = Wire.read();
	int criticalTemperature = Wire.read();

        if(pinNumber == 4)
          value = map(value, 0, 100, 0, 180);
          else
          value = map(value, 0, 100, 0, 90);
	
        

        if(criticalTemperature){
		setServo(pinNumber, value);
		
		switch (pinNumber){
        case 1:
			manualControlFirsServo = 0;
			break;
        
        case 2:
        manualControlSecondServo = 0;
			//valveSecond.write(value);
			break;
        
        case 3:
        manualControlThirdServo = 0;
			//valveThird.write(value);
			break;
        
        case 4:
        manualControlFourthServo = 0;
			//valveFourth.write(value);
			break;
        
        default:
			break;
      
   }
	}
	else{
		switch (pinNumber){
        case 1:
			if(!manualControlFirsServo)
				setServo(pinNumber, value);
			break;
        
        case 2:
      if(!manualControlSecondServo)  
			setServo(pinNumber, value);
			break;
        
        case 3:
      if(!manualControlThirdServo)  
			setServo(pinNumber, value);
			break;
        
        case 4:
      if(!manualControlFourthServo)  
			setServo(pinNumber, value);
			break;
        
        default:
			break;
      
		}
	}
	
}

void setAllServos() {
  	valveFirst.attach(valveFirstPin);
	valveFirst.write(positionFirst + correctionFirst);

	valveSecond.attach(valveSecondPin);
	valveSecond.write(positionSecond + correctionSecond);

	valveThird.attach(valveThirdPin);
	valveThird.write(positionThird + correctionThird);

	valveFourth.attach(valveFourthPin);
	valveFourth.write(positionFourth + correctionFourth);
}
