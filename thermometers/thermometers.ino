#include <Wire.h>
#include <OneWire.h>
#include <SPI.h>
#include <SD.h>

const uint8_t countOfThermometers = 30;
int8_t temperature[countOfThermometers];
uint8_t chosenThermometer;

const int master = 0x01;
const int slaveThermometers = 0x02;

OneWire ds(8);  // One Wire pluggen in 8 pin
byte thermometers[countOfThermometers][8];

int checkTemperature(byte addr[8]);
byte charsToByte(char firstChar, char secondChar);
void readAllAddressOfThermometers();

void setup() {
	readAllAddressOfThermometers();

	Wire.begin(slaveThermometers);
	Wire.onReceive(readWhichThermometerHasToBeChosen); // Przychodzące dane, z informacją, który termometr odczytać
	Wire.onRequest(sendChosenTemperature); // Wysłanie dancyh z temperaturą o danym termometrze
	Serial.begin(9600);
}

void loop() {
	for (int i = 0; i < countOfThermometers; i++) {
		temperature[i] = checkTemperature(thermometers[i]);
		Serial.print("Termometr nr: ");
		Serial.print(i);
		Serial.print(", temperatura: ");
		Serial.print(temperature[i]);
		Serial.print(" adres: ");
		for (int j = 0; j < 8; j++) {
			Serial.print(thermometers[i][j], HEX);
			Serial.print(",");
		}
		Serial.println();
	}

	delay(1000);
}

int checkTemperature(byte addr[8]) {
	byte present = 0;
	byte type_s;
	byte data[12];

	ds.reset();
	ds.select(addr);
	ds.write(0x44, 1);        // start conversion, with parasite power on at the end

	delay(1000);     // maybe 750ms is enough, maybe not
					 // we might do a ds.depower() here, but the reset will take care of it.

	present = ds.reset();
	ds.select(addr);
	ds.write(0xBE);         // Read Scratchpad
	for (byte i = 0; i < 9; i++) {           // we need 9 bytes
		data[i] = ds.read();
	}

	// Convert the data to actual temperature
	// because the result is a 16 bit signed integer, it should
	// be stored to an "int16_t" type, which is always 16 bits
	// even when compiled on a 32 bit processor.
	int16_t raw = (data[1] << 8) | data[0];
	if (type_s) {
		raw = raw << 3; // 9 bit resolution default
		if (data[7] == 0x10) {
			// "count remain" gives full 12 bit resolution
			raw = (raw & 0xFFF0) + 12 - data[6];
		}
	}
	else {
		byte cfg = (data[4] & 0x60);
		// at lower res, the low bits are undefined, so let's zero them
		if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
		else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
		else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
											  //// default is 12 bit resolution, 750 ms conversion time
	}
	return (int)raw / 16.0;
}

byte charsToByte(char firstChar, char secondChar) {
	int num;
	if ((int)secondChar != 45) { // "-"
		char str[2];
		str[0] = firstChar;
		str[1] = secondChar;
		sscanf(str, "%x", &num);
	}
	else {
		char str[1];
		str[0] = firstChar;
		sscanf(str, "%x", &num);
	}

	return num;
}

void readWhichThermometerHasToBeChosen(int numBytes) {
	chosenThermometer = Wire.read(); // odczyt z i2c z którego termometru ba być brany odczyt
}
void sendChosenTemperature() {
	Wire.write(temperature[chosenThermometer - 1]); // wysłanie odpowiedzi z temperaturą na danym termometrze
}

void readAllAddressOfThermometers() {
	File myFile;

	Serial.print("Initializing SD card...");

	if (!SD.begin(4)) {
		Serial.println("initialization failed!");
		return;
	}
	Serial.println("initialization done.");

	myFile = SD.open("term1.txt", FILE_READ);

	if (myFile) {
		Serial.println("Nazwa termometru:");

		for (int indexOfThermometer = 0; indexOfThermometer < countOfThermometers; indexOfThermometer++) {
			char readAddressOfThermometer[23];
			strcpy(readAddressOfThermometer, myFile.readStringUntil('\n').c_str());
			Serial.println(readAddressOfThermometer);
			Serial.println("------------------------");

			int i = 0;
			for (int j = 0; j < 8; j++) {
				thermometers[indexOfThermometer][j] = charsToByte(readAddressOfThermometer[i], readAddressOfThermometer[i + 1]);
				i += 3;
			}
		}

		// close the file:
		myFile.close();
	}
	else {
		// if the file didn't open, print an error:
		Serial.println("error opening");
	}
}

